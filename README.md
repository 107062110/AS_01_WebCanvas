# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    There are many button on the canvas , click to operate the function : reset is to clear all canvas and back to draw ; eraser is to clear the path your mouse move ; draw is to color the path your mouse move ;  line width can change draw,eraser and graphics line width ; color is to change draw,graphics and text color ; click text,font size and font family can type in words ; you can click font size or click it then move the slider by keyboard left right or up down to change the font size ; font family can choose your font family ; circle,rectangle and triangle can draw circle,rectangle and triangle respectively; redo undo can move to before status you prefer ; upload can upload image and download can save you draw. 


### Gitlab page link

    https://107062110.gitlab.io/AS_01_WebCanvas
    
