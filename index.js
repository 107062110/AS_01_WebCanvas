var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var color = document.getElementById("mycolor");
var range = document.getElementById("myrange");
var output = document.getElementById("word_size");
var select = document.getElementById("myselect");
ctx.globalCompositeOperation = "source-over";
var string2 = 'paint';
var shapeis;
ctx.lineWidth = 3;
ctx.font = "20px Arial";
ctx.fillStyle = "black";
canvas.style.cursor = 'url(brush.png), auto';
var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'powderblue', 'black', 'white'];
var size = [3, 5, 10, 20, 30, 40];
var sizeNames = ['default', 'five', 'ten', 'twenty', 'thirty', 'fourty'];
var font_family;
var startPos;
var now_image = new Image();
var undo = new Array();
var redo = [];
var redo_undo_flag = 1;
var now_cursor;

function compareStr() {
  var string1 = "paint";
  return result = string1.localeCompare(string2);
}

function drawWhat() {
  var shape = "rect";
  return result = shape.localeCompare(shapeis);
}



function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

output.innerHTML = range.value;

range.oninput = function () {
  output.innerHTML = this.value;
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x - 20, mousePos.y - 15);
  ctx.stroke();
}

function drawShape(evt) {
  var nowPos = getMousePos(canvas, evt);
  if (drawWhat() == 0) {
    ctx.beginPath();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(now_image, 0, 0);
    ctx.rect(startPos.x, startPos.y, nowPos.x - startPos.x, nowPos.y - startPos.y);
    ctx.stroke();
  } else if (drawWhat() == 1) {
    ctx.beginPath();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(now_image, 0, 0);
    var radius = Math.sqrt((nowPos.x - startPos.x) * (nowPos.x - startPos.x) + (nowPos.y - startPos.y) * (nowPos.y - startPos.y));
    ctx.arc(startPos.x, startPos.y, radius, 0, 2 * Math.PI);
    ctx.stroke();
  }
  else if (drawWhat() == -1) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(now_image, 0, 0);
    var length = Math.sqrt((nowPos.x - startPos.x) * (nowPos.x - startPos.x) + (nowPos.y - startPos.y) * (nowPos.y - startPos.y));
    ctx.beginPath();
    ctx.moveTo(startPos.x, startPos.y);
    ctx.lineTo(nowPos.x, nowPos.y);
    ctx.lineTo(nowPos.x - length, nowPos.y);
    ctx.closePath();
    ctx.stroke();
  }
}

canvas.addEventListener('mousedown', function (evt) {
  now_image.src = canvas.toDataURL();
  undo.push(canvas.toDataURL());
  redo = [];
    if (compareStr() == 0) {
    var mousePos = getMousePos(canvas, evt);
    ctx.fillRect(mousePos.x-20 , mousePos.y-15 , 3,3);
    ctx.beginPath();
    ctx.moveTo(mousePos.x - 20, mousePos.y - 15);
    ctx.strokeStyle = color.value;
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  }
  if (compareStr() == 1) {
    startPos = getMousePos(canvas, evt);
    drawShape(evt);
    ctx.strokeStyle = color.value;
    canvas.addEventListener('mousemove', drawShape, false);
  }
});

canvas.addEventListener('mouseup', function () {
  canvas.removeEventListener('mousemove', mouseMove, false)
  canvas.removeEventListener('mousemove', drawShape, false)
}, false);

document.getElementById('reset').addEventListener('click', function () {
  string2 = 'paint';
  redo_undo_flag = 1;
  ctx.globalCompositeOperation = "source-over";
  ctx.lineWidth = 3;
  canvas.style.cursor = 'url(brush.png), auto';
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

document.getElementById('eraser').addEventListener('click', function (evt) {
  redo_undo_flag = 0;
  now_cursor = "pen";
  string2 = 'paint';
  canvas.style.cursor = 'url(eraser.png), auto';
  ctx.globalCompositeOperation = "destination-out";
}, false);


document.getElementById("mycolor").addEventListener('click', function () {
  redo_undo_flag = 1;
  ctx.globalCompositeOperation = "source-over";
  if (now_cursor=="pen") {canvas.style.cursor = 'url(brush.png), auto';}
  else if (now_cursor=="text") {canvas.style.cursor = text;}
  else if (now_cursor=="tri") {canvas.style.cursor = 'url(tri.png), auto';}
  else if (now_cursor=="rect") {canvas.style.cursor = 'url(rect_cursor.png), auto';}
  else if (now_cursor=="circle") {canvas.style.cursor = 'url(circle.png), auto';}
}, false);

document.getElementById("rect").addEventListener('click', function () {
  redo_undo_flag = 1;
  string2 = 'drawshape';
  shapeis = 'rect';
  now_cursor = "rect";
  canvas.style.cursor = 'url(rect_cursor.png), auto';
  ctx.globalCompositeOperation = "source-over";
}, false);

document.getElementById("circle").addEventListener('click', function () {
  redo_undo_flag = 1;
  string2 = 'drawshape';
  shapeis = 'circle';
  now_cursor = "circle";
  canvas.style.cursor = 'url(circle.png), auto';
  ctx.globalCompositeOperation = "source-over";
}, false);

document.getElementById("tri").addEventListener('click', function () {
  redo_undo_flag = 1;
  string2 = 'drawshape';
  shapeis = 'tri';
  now_cursor = "tri";
  canvas.style.cursor = 'url(tri.png), auto';
  ctx.globalCompositeOperation = "source-over";
}, false);

document.getElementById("pen").addEventListener('click', function () {
  redo_undo_flag = 1;
  string2 = 'paint';
  now_cursor = "pen";
  ctx.globalCompositeOperation = "source-over";
  canvas.style.cursor = 'url(brush.png), auto';
}, false);

function lineWidth(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function () {
    ctx.lineWidth = size[i];
  }, false);
}



function fontSizes() {
  document.getElementById("myrange").addEventListener('click', function () {
    canvas.style.cursor = "text";
    now_cursor = "text";
    string2 = 'text';
    font_size = range.value + "px";
    ctx.font = font_size + " " + font_family;
  }, false);
  document.getElementById("myrange").addEventListener('keyup', function () {
    string2 = 'text';
    font_size = range.value + "px";
    ctx.font = font_size + " " + font_family;
  }, false);

}

document.getElementById("myselect").addEventListener('click', function () {
  canvas.style.cursor = "text";
  now_cursor = "text";
  console.log(select.value);
  string2 = 'text';
  font_family = select.value;
  canvas.style.cursor = text;
  console.log(font_family);
  ctx.font = range.value + "px" + " " + font_family;
}, false);


for (var i = 0; i < size.length; i++) {
  lineWidth(i);
}

for (var i = 0; i < size.length; i++) {
  fontSizes(0);
}


document.getElementById('inp').onchange = function (e) {
  ctx.globalCompositeOperation = "source-over";
  var img = new Image();
  img.onload = draw;
  img.src = URL.createObjectURL(this.files[0]);
  undo.push(canvas.toDataURL());
  redo = [];
};
function draw() {
  ctx.drawImage(this, 0, 0);
  now_image.src = canvas.toDataURL();
  document.getElementById('inp').value = null;
  if(redo_undo_flag){
    ctx.globalCompositeOperation="source-over";
  }else {
    ctx.globalCompositeOperation="destination-out";
  }

}


canvas.addEventListener('click', function (evt) {
  if (compareStr() == -1) {
    var mousePos = getMousePos(canvas, evt);
    mousePos.x = String(mousePos.x + 100) + "px";
    mousePos.y = String(mousePos.y + 70) + "px";
    var text = document.createElement('input');
    text.style.position = 'absolute';
    text.style.left = mousePos.x;
    text.style.top = mousePos.y;
    ctx.fillStyle = color.value;
    document.getElementById('write').appendChild(text);
    text.focus();
    text.addEventListener('keyup', function (evt) {
      if (evt.keyCode == 13) {
        var x_str_length = mousePos.x.length;
        var y_str_length = mousePos.y.length;
        var x = Number(mousePos.x.substr(0, x_str_length - 2));
        var y = Number(mousePos.y.substr(0, y_str_length - 2));
        ctx.fillText(text.value, x - 130, y - 90);
        text.remove();
        now_image.src = canvas.toDataURL();
        undo.push(canvas.toDataURL());
        redo = [];
      }
    })
    canvas.addEventListener('click', function () {
      text.remove();
    })
    window.addEventListener('mouseup', function () {
      text.remove();
    })
  }
}, false);
document.getElementById("text").addEventListener('click', function (evt) {
  redo_undo_flag = 1;
  string2 = 'text';
  now_cursor = "text";
  canvas.style.cursor = "text";
  ctx.globalCompositeOperation = "source-over";
}, false);

document.getElementById("undo").addEventListener('click', function (evt) {
  console.log(redo_undo_flag);
  console.log('*****');
  var draw = new Image();
  if (undo.length != 0) {
    ctx.globalCompositeOperation = "source-over";
    redo.push(canvas.toDataURL());
    draw.src = undo.pop();
    draw.onload = function () {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(draw, 0, 0);
      if (redo_undo_flag) {
        ctx.globalCompositeOperation = "source-over";
      } else {
        ctx.globalCompositeOperation = "destination-out";
      }
    }
  }
}, false);

document.getElementById("redo").addEventListener('click', function (evt) {
  console.log(redo_undo_flag);
  var draw = new Image();
  if (redo.length != 0) {
    ctx.globalCompositeOperation = "source-over";
    undo.push(canvas.toDataURL());
    draw.src = redo.pop();
    draw.onload = function () {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(draw, 0, 0);
      if(redo_undo_flag){
        ctx.globalCompositeOperation="source-over";
      }else {
        ctx.globalCompositeOperation="destination-out";
      }
    }
  }
}, false);

